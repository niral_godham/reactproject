import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import images1 from './images/gallery-img-01.jpg';
import images2 from './images/gallery-img-02.jpg';
import images3 from './images/gallery-img-03.jpg';
import images4 from './images/gallery-img-04.jpg';
import images5 from './images/gallery-img-05.jpg';
import images6 from './images/gallery-img-06.jpg';

class Gallery extends Component{
    render(){
        return(
    <section className="mw9 mw7 center pa3 ph5-ns">
        <p className="tc fw7 Avenir f2 lh-copy">Gallery</p>
        <div className="mw9 center ph3-ns">
            <div className="cf ph2-ns">
                <div class="fl w-100 w-third-ns pa2">
                    <img src={images1} className="link dim orange b"/>
                </div>
                <div className="fl w-100 w-third-ns pa2">
                    <img src={images2} className="link dim orange b"/>
                </div>
                <div className="fl w-100 w-third-ns pa2">
                    <img src={images3} className="link dim orange b" /> 
                </div>
            </div>
        </div>

        <div className="mw9 center ph3-ns">
            <div className="cf ph2-ns">
                <div className="fl w-100 w-third-ns pa2">
                    <img src={images4} className="link dim orange b"/>
                </div>
                <div className="fl w-100 w-third-ns pa2">
                    <img src={images5} className="link dim orange b"/>
                </div>
                <div className="fl w-100 w-third-ns pa2">
                    <img src={images6} className="link dim orange b"/> 
                </div>
            </div>
        </div>

    </section>
        );
    }
}
export default Gallery; 


