import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import {Route, Switch,Link} from 'react-router-dom';
import Slider from './Slider';
import About from './About';
import Gallery from './Gallery';
import Contact from './Contact';
class Header extends Component
{
    render (){
        return(
            
        <div>
           
            <nav class="dt w-100 border-box pa3 ph5-ns">
                <a class="dtc v-mid mid-gray link dim w-25" href="#" title="Home">
                    <img src="http://tachyons.io/img/logo.jpg" class="dib w2 h2 br-100" alt="Site Name"/>
                </a>
            <div class="dtc v-mid w-75 tr"> 
           
            <Link class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" to="/">Home</Link>
            <Link class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" to="Gallery">Gallery</Link>
            <Link class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" to="about">About</Link>
            <Link class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" to="contact">Contact</Link>
            </div>
            
    
           
         </nav>
         <Switch>
                <Route path="/" exact component={Slider}/>
                <Route path="/gallery" component={Gallery}/>  
                <Route path="/about" component={About}/>
                <Route path="/contact" component={Contact}/>
        </Switch>
      
    </div>    
      
        
    );
        }
}
export default Header;