import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './Header';
import Slider from './Slider';
import About from './About';
import Gallery from './Gallery';
import Contact from './Contact';
import * as serviceWorker from './serviceWorker';
import Imgpart from './Imgpart';
import {BrowserRouter} from 'react-router-dom';
ReactDOM.render(
  
  <React.StrictMode>
    <BrowserRouter>
    <Header />
   
    </BrowserRouter>
    
  </React.StrictMode>,
 
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
