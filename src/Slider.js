import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import images from './images/slider-03.jpg';
class Slider extends Component{
    render(){
        return(
            <div>
                <img src={images}/>
            </div>
        );
    }
}
export default Slider; 