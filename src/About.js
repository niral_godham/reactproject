import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import images from './images/about-img.jpg';
class About extends Component{
    render(){
        return(
    <section className="mw9 mw7 center pa3 ph5-ns">
        <div className="mw9 center ph3-ns">
            <div className="cf ph2-ns">
                <div className="fl w-100 w-50-ns pa2">
                    <p className="tl fw5 Avenir f4 lh-copy">
                        <h2>Welcome To <span className="orange">Live Dinner Restaurant</span></h2>
                    <h3 class="tl fw7 Avenir">Little Story</h3>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Pellentesque auctor suscipit feugiat. Ut at pellentesque ante,
                        sed convallis arcu. Nullam facilisis, eros in eleifend luctus, odio ante
                        sodales augue, eget lacinia lectus erat et sem.
                        <br/><br/>
                        Sed semper orci sit amet porta placerat. Etiam quis finibus eros. 
                        Sed aliquam metus lorem, a pellentesque tellus pretium a. Nulla 
                        placerat elit in justo vestibulum, et maximus sem pulvinar.
                        
                        </p>
                </div>
                <div className="fl w-100 w-50-ns pa2">
                   <img src={images}/>
                </div>
            </div>
        </div>
        </section>
        );
    }
}
export default About; 


